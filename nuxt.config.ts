import { defineNuxtConfig } from 'nuxt'

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  buildModules: [
    '@nuxtjs/google-fonts'
  ],
  build: {
    postcss: {
      postcssOptions: {
        plugins: {
          tailwindcss: {},
          autoprefixer: {},
        },
      },
    },
  },
  css: [
    "~/assets/css/tailwind.css"
  ],
  googleFonts: {
    display: 'swap',
    // download: true,
    // overwriting: false,
    families: {
      Roboto: true,
      'Josefin+Sans': true,
      Lato: [100, 300],
      Anton: [400],
      Raleway: {
        wght: [100, 400],
        ital: [100]
      },
    }
  }
})
